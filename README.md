# hgwiki

![](https://img.shields.io/badge/written%20in-PHP-blue)

A web-based wiki system using Mercurial as a backing store.

This is a quick experiment in using Mercurial as a wiki backing store, in order to take advantage of its built-in history, diffing and article management.

## Features

- Create and edit articles
- View article history with timestamps and compare specific revision numbers
- List all articles


## Download

- [⬇️ hgwiki_r02.zip](dist-archive/hgwiki_r02.zip) *(2.02 KiB)*
